export default function CardItem(props) {
  const { img, name } = props;
  return (
    <div className="flex-item">
      <div className="c-item">
        <figure className="c-item__img">
          <img src={img} alt="card-item-img" />
        </figure>
        <div className="c-item__content">{name}</div>
      </div>
    </div>
  );
}
