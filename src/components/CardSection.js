import "./CardSection.css";

export default function CardSection({ children, name }) {
  return (
    <div className="c-section">
      <div className="c-section__head">
        <h3>{name}</h3>
      </div>
      <div className="c-section__content">{children}</div>
    </div>
  );
}
