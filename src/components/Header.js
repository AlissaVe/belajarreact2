import "./Header.css";

function Header(props) {
  return (
    <div className="m-header">
      <div className="m-header__btn">
        <svg width="24" height="24" fill="currentColor" viewBox="0 0 16 16">
          <path
            fill="#838383"
            fill-rule="evenodd"
            d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"
          />
        </svg>
      </div>
      <div className="m-header__form">
        <input type="text" className="input-control" placeholder="Search" />
      </div>
    </div>
  );
}

export default Header;
