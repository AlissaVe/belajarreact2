import { StrictMode } from "react";
import ReactDOM from "react-dom";

import "./styles.css";
import App from "./App";
import TryComponent from "./Belajar2/TryComponent";

// document.getElementById('root');
// document.querySelector('#root');
// document.querySelector('.root');
// document.getElementsByClassName('root');

const rootElement = document.getElementById("root");
ReactDOM.render(
  <StrictMode>
    <App />
  </StrictMode>,
  rootElement
);
