

// statefull component : menulis komponen menggunakan class
import React, { Component } from 'react';

class StatefulComponent extends Component {
    // state di class component
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <button type="button">Button {this.props.name}</button>
            </div>
        )
    }
}


// stateless component
// -- functional komponen nulis komponennya
function StatelessComponent(props) {
    return ( 
        <div>
            <h1>Hello Alisa {props.name}</h1>
        </div>
    );
}

export { StatefulComponent, StatelessComponent };


// versi ES5
function fungsiBiasa() {

}

// versi ES6
const fungsiBaruSemenjakES6 = () => {

}
