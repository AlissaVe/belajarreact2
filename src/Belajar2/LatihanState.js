

// statefull component : menulis komponen menggunakan class
import React, { Component, useEffect } from 'react';

class CounterStatefullComponent extends Component {
    // state di class component
    constructor(props) {
        super(props);
        this.state = {
            counter: 0,
        }
        // this.incrementHandler = this.incrementHandler.bind(this);
        // this.decrementHandler = this.decrementHandler.bind(this);
    }

    incrementHandler = () => {
        this.setState({
            counter: this.state.counter + 1
        });
    }
    
    decrementHandler = () => {
        this.setState({
            counter: this.state.counter - 1
        });
    }

    // ngelog perubahan
    // componentDidUpdate() {
    //     console.log('render did update');
    // }
    render() {
        // destructuring
        const { counter } = this.state;

        return (
            <div>
                <h1>Counter Statefull Component</h1>
                <div style={{ display: 'flex' }}>
                    <button type="button" onClick={this.decrementHandler}>Kurang</button>
                    <div style={{ margin: '0 10px' }}>{counter}</div>
                    <button type="button" onClick={this.incrementHandler}>Tambah</button>
                </div>
            </div>
        )
    }
}


// stateless component
// -- functional komponen nulis komponennya
function CounterStatelessComponent(props) {
    // konsep HOOKS
    const [counter, setCounter] = React.useState(0);

    const decrementHandler = () => {
        setCounter(counter - 1);
    }

    useEffect(() => {
        console.log(counter);
    });

    return ( 
        <div>
            <h1>Counter Stateless Component</h1>
            <div style={{ display: 'flex' }}>
                <button type="button" onClick={decrementHandler}>Kurang</button>
                <div style={{ margin: '0 10px' }}>{counter}</div>
                <button type="button" onClick={() => setCounter(counter + 1)}>Tambah</button>
            </div>
        </div>
    );
}

export { CounterStatefullComponent, CounterStatelessComponent };
