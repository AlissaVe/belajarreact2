import CardSection from "./components/CardSection";
import CardItem from "./components/CardItem";
import Header from "./components/Header";
import Footer from "./Footer";

import "./styles/global.css";
import CardList from "./components/CardList";

// import { StatefulComponent, StatelessComponent } from './Belajar2/TryComponent.js';
import { CounterStatefullComponent, CounterStatelessComponent } from './Belajar2/LatihanState.js';

export default function App() {
  return (
    <div className="mobile-container" style={{ padding: '4rem' }}>
      {/* <StatefulComponent name="alissa" />
      <StatelessComponent name="Breaker" /> */}

      <CounterStatefullComponent />
      <br />
      <hr style={{ borderColor: '#fff' }} />
      <br />
      <CounterStatelessComponent />

      {/* <Header name="halo bro" />
      <br />
      <main style={{ paddingLeft: "15px", paddingRight: "15px" }}>
        <CardSection name="Category">
          <CardList>
            <CardItem img="http://via.placeholder.com/60x60" name="Designer" />
            <CardItem
              img="http://via.placeholder.com/60x60"
              name="Administrator"
            />
            <CardItem
              img="http://via.placeholder.com/60x60"
              name="Programmer"
            />
          </CardList>
        </CardSection>
        <br />
        <CardSection name="Top 50 Jobs">
          <CardList>
            <CardItem
              img="http://via.placeholder.com/60x60"
              name="UI/UX Designer"
            />
            <CardItem
              img="http://via.placeholder.com/60x60"
              name="Administrator"
            />
            <CardItem
              img="http://via.placeholder.com/60x60"
              name="Programmer"
            />
          </CardList>
        </CardSection>
      </main> */}
      {/* <Footer /> */}
    </div>
  );
}
